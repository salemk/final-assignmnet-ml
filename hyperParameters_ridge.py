import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import KFold
from sklearn.linear_model import Ridge

station_id = 21
# station_id = 107
plt.rc('font', size=18)
plt.rcParams['figure.constrained_layout.use'] = True

df = pd.read_csv("datasets/dublinBikes_" + str(station_id) + ".csv")

df.index = pd.to_datetime((df['TIME']), format='%Y-%m-%d %H:%M:%S')
bikes = df['OCCUPANCY']
monday = df['Monday']
saturday = df['Saturday']
sunday = df['Sunday']
weekdays = df['Tue-Fri']
day_n = (df['DAY_NUMBER'] + 1) / 7
time = df['TIME']

ST_stride = 1  # short trend stride, every 10 min
week_stride = 24 * 12 * 7  # week trend stride, same time a week ago
day_stride = 24 * 12  # day trend stride, same time a day ago


def short_trend(df, q, lag, dd):
    df_as_np = df.to_numpy()
    short_temp = df[lag * dd - ST_stride * (lag - 1):len(df_as_np) - q - ST_stride * (lag - 1)]
    for i in range(1, lag):
        short_temp2 = df[lag * dd - ST_stride * (lag - 1) + i * ST_stride:len(df_as_np) - q - ST_stride * (
                lag - 1) + i * ST_stride]
        short_temp = np.column_stack((short_temp, short_temp2))
    return short_temp


def week(df, q, lag, dd):
    df_as_np = df.to_numpy()
    week_temp = df[lag * dd + q - week_stride * (lag):len(df_as_np) - week_stride * (lag)]
    for i in range(1, lag):
        week_temp2 = df[lag * dd + q - week_stride * (lag - i):len(df_as_np) - week_stride * (lag - i)]
        week_temp = np.column_stack((week_temp, week_temp2))
    return week_temp


def day(df, q, lag, dd):
    df_as_np = df.to_numpy()
    day_temp = df[lag * dd + q - day_stride * (lag):len(df_as_np) - day_stride * (lag)]
    for i in range(1, lag):
        day_temp2 = df[lag * dd + q - day_stride * (lag - i):len(df_as_np) - day_stride * (lag - i)]
        day_temp = np.column_stack((day_temp, day_temp2))
    return day_temp


# q_val = 2
# q_val = 6
q_val = 12
lag_val = 2  # window size
dd_val = 7 * 288  # because I'm using weekly seasonality this gets back exactly one week back at the same time

bikes_y = bikes[lag_val * dd_val + q_val:]
x_short = short_trend(bikes, q_val, lag_val, dd_val)
x_week = week(bikes, q_val, lag_val, dd_val)
x_day = day(bikes, q_val, lag_val, dd_val)
timeDayNum = day_n[lag_val * dd_val + q_val:]
timeMonday = monday[lag_val * dd_val + q_val:]
timeWeekdays= weekdays[lag_val * dd_val + q_val:]
timeSaturday = saturday[lag_val * dd_val + q_val:]
timeSunday = sunday[lag_val * dd_val + q_val:]
x_fea = np.column_stack((x_week, x_short))
# x_fea = np.column_stack((x_week, x_day, x_short))
# x_fea = np.column_stack((x_week,x_day, x_short,timeDayNum))
# x_fea = np.column_stack((x_week,x_day, x_short,timeMonday,timeWeekdays,timeSaturday,timeSunday))
print(len(bikes_y))
print(len(x_short))

times_y = time[lag_val * dd_val + q_val:]
t_short = short_trend(time, q_val, lag_val, dd_val)
t_week = week(time, q_val, lag_val, dd_val)
t_day = day(time, q_val, lag_val, dd_val)
t_fea = np.column_stack((t_week, t_day, t_short))
print("time")
print(t_fea)
print(times_y)

size_train = int(len(x_fea) * 0.8)
X_train, y_train = x_fea[:size_train], bikes_y[:size_train]
X_test, y_test = x_fea[size_train:], bikes_y[size_train:]


def plotResults(test_result_model, name):
    plt.figure(figsize=(30, 20))
    plt.scatter(test_result_model['Target'].index, test_result_model['Target'], color='blue', label="actual")
    plt.scatter(test_result_model['Target'].index, test_result_model['Predictions'], color='orange', label="pred")
    plt.xlabel("time")
    plt.ylabel("occupancy")
    plt.legend(loc='upper right')
    plt.title(name + " _ Station " + str(station_id))
    plt.show()


def printScore(test_result_model, name):
    print(name + " -  mean squared error: %.4f" % mean_squared_error(test_result_model['Target'],
                                                                     test_result_model['Predictions']))


mean_error_test = []
std_error_test = []
mean_error_train = []
std_error_train = []
Ci_range = [0.0001, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10, 50, 100]
# 21_q2_mse= [0.05618350505041653, 0.011442795458619448, 0.004593747522216685, 0.0038853850067302603, 0.0033361458139067697, 0.0031885275880106285, 0.003014068745280592, 0.002996781734645146, 0.0029889252239490988, 0.0029885142235404924, 0.0029882990683265864, 0.00298827959415337]
# 21_q6_mse= [0.058560016521273725, 0.01711615438747555, 0.011861627344639128, 0.011328967509455105, 0.010788304204037688, 0.010606724096905147, 0.010383285971588142, 0.0103594146367996, 0.010347161371730775, 0.010346294219534867, 0.010345733589276266, 0.010345672210892035]
# 21_q12_mse= [0.06208254580038806, 0.02469209501493716, 0.021398200692896267, 0.021164354634903522, 0.020844924431819456, 0.02069213868384418, 0.02048611355373286, 0.02046159646704472, 0.020447358790635212, 0.020446118749357155, 0.020445236240777438, 0.020445133104394598]
# 107_q2_mse= [0.045341635834697754, 0.01208395774437313, 0.0025522911698067545, 0.001628062726713184, 0.001159558482659958, 0.001098680860584168, 0.0009655060593955971, 0.0009301474448266924, 0.0009053718438840711, 0.0009038514503579891, 0.0009031813207657115, 0.0009031377627274032]
# 107_q6_mse= [0.04601012942577249, 0.01427925863623894, 0.005318951071321543, 0.0044276189005786224, 0.003918621726983912, 0.0038340429895325234, 0.0036474452378235994, 0.003598126919213977, 0.0035630765536163087, 0.0035608009884441905, 0.0035597156917521974, 0.0035596342210774214]
# 107_q12_mse= [0.04714382241026967, 0.017939792060889182, 0.009974768171355035, 0.009172628832232113, 0.008650017224737227, 0.00853896494456975, 0.008284447749926996, 0.008216685278575605, 0.00816817000845924, 0.008164958775283188, 0.008163392388292551, 0.008163270588984401]
for Ci in Ci_range:
    rigid = Ridge(alpha=1 / (2 * Ci))
    temp_test = []
    temp_train = []
    cv = KFold(n_splits=5)
    for train, test in cv.split(X_train):
        rigid.fit(X_train[train], y_train[train])
        ypred = rigid.predict(X_train[test])
        ypred_train = rigid.predict(X_train[train])
        temp_test.append(mean_squared_error(y_train[test], ypred))
        temp_train.append(mean_squared_error(y_train[train], ypred_train))
    mean_error_test.append(np.array(temp_test).mean())
    std_error_test.append(np.array(temp_test).std())
    mean_error_train.append(np.array(temp_train).mean())
    std_error_train.append(np.array(temp_train).std())

print(mean_error_test)
plt.errorbar(Ci_range, mean_error_test, yerr=std_error_test, label="test")
plt.errorbar(Ci_range, mean_error_train, yerr=std_error_train,label="train")
plt.xlabel('Ci');
plt.ylabel('Mean square error')
plt.title("Ridge")
plt.legend()
plt.show()

C = 0.5
# C = 1.0
rigid = Ridge(alpha=1 / (2 * C)).fit(X_train, y_train)
print("coef")
print(rigid.intercept_, rigid.coef_)

test_predictions = rigid.predict(X_test)
test_results = pd.DataFrame(data={'Predictions': test_predictions, 'Target': y_test})
print(test_results)

printScore(test_results, "Ridge")
plotResults(test_results, "Ridge")




