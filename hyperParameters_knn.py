import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import KFold
from sklearn.neighbors import KNeighborsRegressor

station_id = 21
# station_id = 107
plt.rc('font', size=18)
plt.rcParams['figure.constrained_layout.use'] = True

df = pd.read_csv("datasets/dublinBikes_" + str(station_id) + ".csv")

df.index = pd.to_datetime((df['TIME']), format='%Y-%m-%d %H:%M:%S')
bikes = df['OCCUPANCY']
monday = df['Monday']
saturday = df['Saturday']
sunday = df['Sunday']
weekdays = df['Tue-Fri']
day_n = (df['DAY_NUMBER'] + 1) / 7
time = df['TIME']

ST_stride = 1  # short trend stride, every 10 min
week_stride = 24 * 12 * 7  # week trend stride, same time a week ago
day_stride = 24 * 12  # day trend stride, same time a day ago


def short_trend(df, q, lag, dd):
    df_as_np = df.to_numpy()
    short_temp = df[lag * dd - ST_stride * (lag - 1):len(df_as_np) - q - ST_stride * (lag - 1)]
    for i in range(1, lag):
        short_temp2 = df[lag * dd - ST_stride * (lag - 1) + i * ST_stride:len(df_as_np) - q - ST_stride * (
                lag - 1) + i * ST_stride]
        short_temp = np.column_stack((short_temp, short_temp2))
    return short_temp


def week(df, q, lag, dd):
    df_as_np = df.to_numpy()
    week_temp = df[lag * dd + q - week_stride * (lag):len(df_as_np) - week_stride * (lag)]
    for i in range(1, lag):
        week_temp2 = df[lag * dd + q - week_stride * (lag - i):len(df_as_np) - week_stride * (lag - i)]
        week_temp = np.column_stack((week_temp, week_temp2))
    return week_temp


def day(df, q, lag, dd):
    df_as_np = df.to_numpy()
    day_temp = df[lag * dd + q - day_stride * (lag):len(df_as_np) - day_stride * (lag)]
    for i in range(1, lag):
        day_temp2 = df[lag * dd + q - day_stride * (lag - i):len(df_as_np) - day_stride * (lag - i)]
        day_temp = np.column_stack((day_temp, day_temp2))
    return day_temp


# q_val = 2
# q_val = 6
q_val = 12
lag_val = 2  # window size
dd_val = 7 * 288  # because I'm using weekly seasonality this gets back exactly one week back at the same time

bikes_y = bikes[lag_val * dd_val + q_val:]
x_short = short_trend(bikes, q_val, lag_val, dd_val)
x_week = week(bikes, q_val, lag_val, dd_val)
x_day = day(bikes, q_val, lag_val, dd_val)
timeDayNum = day_n[lag_val * dd_val + q_val:]
timeMonday = monday[lag_val * dd_val + q_val:]
timeWeekdays= weekdays[lag_val * dd_val + q_val:]
timeSaturday = saturday[lag_val * dd_val + q_val:]
timeSunday = sunday[lag_val * dd_val + q_val:]
# x_fea = np.column_stack((x_week, x_short))
x_fea = np.column_stack((x_week, x_day, x_short))
# x_fea = np.column_stack((x_week,x_day, x_short,timeDayNum))
# x_fea = np.column_stack((x_week,x_day, x_short,timeMonday,timeWeekdays,timeSaturday,timeSunday))


size_train = int(len(x_fea) * 0.8)
X_train, y_train = x_fea[:size_train], bikes_y[:size_train]
X_test, y_test = x_fea[size_train:], bikes_y[size_train:]


def plotResults(test_result_model, name):
    plt.figure(figsize=(30, 20))
    plt.scatter(test_result_model['Target'].index, test_result_model['Predictions'], color='orange', label="pred")
    plt.scatter(test_result_model['Target'].index, test_result_model['Target'], color='blue', label="actual")
    plt.xlabel("time")
    plt.ylabel("occupancy")
    plt.legend(loc='upper right')
    plt.title(name + " _ Station " + str(station_id))
    plt.show()


def printScore(test_result_model, name):
    print(name + " -  mean squared error: %.2f" % mean_squared_error(test_result_model['Target'],
                                                                     test_result_model['Predictions']))


# getting the best k value and weight
k_range = [5, 10, 25, 50, 75, 100, 125, 150, 200, 300, 500]
k_weigh = ['uniform', 'distance']

plt.figure()
for kw in k_weigh:
    mean_error_test = []
    std_error_test = []
    mean_error_train = []
    std_error_train = []
    for k in k_range:
        kmodel = KNeighborsRegressor(n_neighbors=k, weights=kw)
        temp_uni_test = []
        temp_uni_train = []
        cv = KFold(n_splits=5)
        for train, test in cv.split(X_train):
            kmodel.fit(X_train[train], y_train[train])
            y_pred_knn = kmodel.predict(X_train[test])
            y_pred_knn_train = kmodel.predict(X_train[train])
            temp_uni_test.append(mean_squared_error(y_train[test], y_pred_knn))
            temp_uni_train.append(mean_squared_error(y_train[train], y_pred_knn_train))
        mean_error_test.append(np.array(temp_uni_test).mean())
        std_error_test.append(np.array(temp_uni_test).std())
        mean_error_train.append(np.array(temp_uni_train).mean())
        std_error_train.append(np.array(temp_uni_train).std())
    print(mean_error_test)
    print(mean_error_train)
    plt.errorbar(k_range, mean_error_test, yerr=std_error_test, label=kw+"_test")
    plt.errorbar(k_range, mean_error_train, yerr=std_error_train, label=kw+"_train")
    plt.xlabel('K val')
    plt.ylabel('Mean square error')
    plt.legend()
    plt.show()

knn = KNeighborsRegressor(n_neighbors=100, weights='uniform')
knn.fit(X_train, y_train)
yPred_knn = knn.predict(X_test)

test_results_knn = pd.DataFrame(data={'Predictions': yPred_knn, 'Target': y_test})
print(test_results_knn)

printScore(test_results_knn, "KNN")
plotResults(test_results_knn, "KNN")


