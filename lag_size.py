import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import Ridge

# station_id = 21
# c_ridge = 0.5  # station21
station_id = 107
c_ridge= 1.0 #station107

plt.rc('font', size=18)

df = pd.read_csv("datasets/dublinBikes_" + str(station_id) + ".csv")

df.index = pd.to_datetime((df['TIME']), format='%Y-%m-%d %H:%M:%S')
bikes = df['OCCUPANCY']
monday = df['Monday']
saturday = df['Saturday']
sunday = df['Sunday']
weekdays = df['Tue-Fri']
day_n = (df['DAY_NUMBER'] + 1) / 7
time = df['TIME']

ST_stride = 1  # short trend stride, every 10 min
week_stride = 24 * 12 * 7  # week trend stride, same time a week ago
day_stride = 24 * 12   # day trend stride, same time a day ago

def short_trend(df, q, lag, dd):
    df_as_np = df.to_numpy()
    short_temp = df[lag * dd - ST_stride * (lag - 1):len(df_as_np) - q - ST_stride * (lag - 1)]
    for i in range(1, lag):
        short_temp2 = df[lag * dd - ST_stride * (lag - 1) + i * ST_stride:len(df_as_np) - q - ST_stride * (
                lag - 1) + i * ST_stride]
        short_temp = np.column_stack((short_temp, short_temp2))
    return short_temp


def week(df, q, lag, dd):
    df_as_np = df.to_numpy()
    week_temp = df[lag * dd + q - week_stride * (lag):len(df_as_np) - week_stride * (lag)]
    for i in range(1, lag):
        week_temp2 = df[lag * dd + q - week_stride * (lag - i):len(df_as_np) - week_stride * (lag - i)]
        week_temp = np.column_stack((week_temp, week_temp2))
    return week_temp


def day(df, q, lag, dd):
    df_as_np = df.to_numpy()
    day_temp = df[lag * dd + q - day_stride * (lag):len(df_as_np) - day_stride * (lag)]
    for i in range(1, lag):
        day_temp2 = df[lag * dd + q - day_stride * (lag - i):len(df_as_np) - day_stride * (lag - i)]
        day_temp = np.column_stack((day_temp, day_temp2))
    return day_temp


def returnMean(test_result_model):
    return mean_squared_error(test_result_model['Target'],
                       test_result_model[
                           'Predictions'])


k_weight = 'uniform'

# uncomment below to predict 10min into the future
# q_val = 2
# k_val = 25  # station21
# k_val=10 #station107

# uncomment below to predict half an hour into the future
# q_val = 6
# k_val=50 #station21
# k_val=100 #station107

# uncomment below to predict an hour into the future
q_val = 12
k_val=100

lag_range = [1,2, 3, 4, 5]
mse_dict = {}
mse_dict['ridge']=[]
mse_dict['knn']=[]

mse_dict_test = {}
mse_dict_test['ridge']=[]
mse_dict_test['knn']=[]

for lag_val in lag_range:
    dd_val = 7 * 288
    bikes_y = bikes[lag_val * dd_val + q_val:]
    x_short = short_trend(bikes, q_val, lag_val, dd_val)
    x_week = week(bikes, q_val, lag_val, dd_val)
    x_fea = np.column_stack((x_week, x_short))

    size_train = int(len(x_fea) * 0.8)
    X_train, y_train = x_fea[:size_train], bikes_y[:size_train]
    X_test, y_test = x_fea[size_train:], bikes_y[size_train:]

    rigid = Ridge(alpha=1 / (2 * c_ridge)).fit(X_train, y_train)
    print("coef")
    print(rigid.intercept_, rigid.coef_)

    test_predictions = rigid.predict(X_test)
    train_predictions = rigid.predict(X_train)
    test_results = pd.DataFrame(data={'Predictions': test_predictions, 'Target': y_test})
    train_results = pd.DataFrame(data={'Predictions': train_predictions, 'Target': y_train})
    print(test_results)

    print(str(lag_val) + "Test Ridge -  mean squared error: %.2f" % returnMean(test_results))
    print(str(lag_val) + "Train Ridge -  mean squared error: %.2f" % returnMean(train_results))
    print(returnMean(test_results))
    mse_dict['ridge'].append(returnMean(train_results))
    mse_dict_test['ridge'].append(returnMean(test_results))

    knn = KNeighborsRegressor(n_neighbors=k_val, weights=k_weight)
    knn.fit(X_train, y_train)
    yPred_knn = knn.predict(X_test)
    yPred_knn_train = knn.predict(X_train)

    test_results_knn = pd.DataFrame(data={'Predictions': yPred_knn, 'Target': y_test})
    train_results_knn = pd.DataFrame(data={'Predictions': yPred_knn_train, 'Target': y_train})
    print(test_results_knn)

    print(str(lag_val) + "Test KNN -  mean squared error: %.2f" % returnMean(test_results_knn))
    print(str(lag_val) + "Train KNN -  mean squared error: %.2f" % returnMean(train_results_knn))
    mse_dict['knn'].append(returnMean(train_results_knn))
    mse_dict_test['knn'].append(returnMean(test_results_knn))


plt.figure(figsize=(30, 20))
plt.title("Station " + str(station_id))
plt.plot(lag_range, mse_dict['ridge'], label="train ridge")
plt.plot(lag_range, mse_dict_test['ridge'], label="test ridge")
plt.plot(lag_range, mse_dict['knn'], label="train knn")
plt.plot(lag_range, mse_dict_test['knn'], label="test knn")
plt.locator_params(axis="both", integer=True, tight=True)
plt.xlabel("lag")
plt.ylabel("MSE")
plt.legend(loc='upper right')
plt.show()

