import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import KFold
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import Ridge, Lasso

# station_id = 21
station_id = 107
plt.rc('font', size=18)
plt.rcParams['figure.constrained_layout.use'] = True

df = pd.read_csv("datasets/dublinBikes_" + str(station_id) + ".csv")

df.index = pd.to_datetime((df['TIME']), format='%Y-%m-%d %H:%M:%S')
bikes = df['OCCUPANCY']
day_n = (df['DAY_NUMBER'] + 1) / 7
time = df['TIME']

ST_stride = 1  # short trend stride, every 10 min
week_stride = 24 * 12 * 7  # week trend stride, same time a week ago
day_stride = 24 * 12  # day trend stride, same time a day ago


def short_trend(df, q, lag, dd):
    df_as_np = df.to_numpy()
    short_temp = df[lag * dd - ST_stride * (lag - 1):len(df_as_np) - q - ST_stride * (lag - 1)]
    for i in range(1, lag):
        short_temp2 = df[lag * dd - ST_stride * (lag - 1) + i * ST_stride:len(df_as_np) - q - ST_stride * (
                lag - 1) + i * ST_stride]
        short_temp = np.column_stack((short_temp, short_temp2))
    return short_temp


def week(df, q, lag, dd):
    df_as_np = df.to_numpy()
    week_temp = df[lag * dd + q - week_stride * (lag):len(df_as_np) - week_stride * (lag)]
    for i in range(1, lag):
        week_temp2 = df[lag * dd + q - week_stride * (lag - i):len(df_as_np) - week_stride * (lag - i)]
        week_temp = np.column_stack((week_temp, week_temp2))
    return week_temp


def day(df, q, lag, dd):
    df_as_np = df.to_numpy()
    day_temp = df[lag * dd + q - day_stride * (lag):len(df_as_np) - day_stride * (lag)]
    for i in range(1, lag):
        day_temp2 = df[lag * dd + q - day_stride * (lag - i):len(df_as_np) - day_stride * (lag - i)]
        day_temp = np.column_stack((day_temp, day_temp2))
    return day_temp


# q_val = 2
q_val = 6
# q_val = 12
lag_val = 2  # window size
dd_val = 7 * 288  # because I'm using weekly seasonality this gets back exactly one week back at the same time

bikes_y = bikes[lag_val * dd_val + q_val:]
x_short = short_trend(bikes, q_val, lag_val, dd_val)
x_week = week(bikes, q_val, lag_val, dd_val)
x_day = day(bikes, q_val, lag_val, dd_val)
timeDayNum = day_n[lag_val * dd_val + q_val:]
# x_fea = np.column_stack((x_week, x_short))
x_fea = np.column_stack((x_week, x_day, x_short))
# x_fea = np.column_stack((x_week,x_day, x_short,timeDayNum))
print(len(bikes_y))
print(len(x_short))

times_y = time[lag_val * dd_val + q_val:]
t_short = short_trend(time, q_val, lag_val, dd_val)
t_week = week(time, q_val, lag_val, dd_val)
t_day = day(time, q_val, lag_val, dd_val)
t_fea = np.column_stack((t_week, t_day, t_short))
print("time")
print(t_fea)
print(times_y)

size_train = int(len(x_fea) * 0.8)
X_train, y_train = x_fea[:size_train], bikes_y[:size_train]
X_test, y_test = x_fea[size_train:], bikes_y[size_train:]


def plotResults(test_result_model, name):
    plt.figure(figsize=(30, 20))
    plt.scatter(test_result_model['Target'].index, test_result_model['Predictions'], color='orange', label="pred")
    plt.scatter(test_result_model['Target'].index, test_result_model['Target'], color='blue', label="actual")
    plt.xlabel("time")
    plt.ylabel("occupancy")
    plt.legend(loc='upper right')
    plt.title(name + " _ Station " + str(station_id))
    plt.show()


def printScore(test_result_model, name):
    print(name + " -  mean squared error: %.2f" % mean_squared_error(test_result_model['Target'],
                                                                     test_result_model['Predictions']))
    print(name + " - r2 square: %.2f" % r2_score(test_result_model['Target'], test_result_model['Predictions']))

# lasso
mean_error_test = []
std_error_test = []
mean_error_train = []
std_error_train = []
Ci_range_l = [0.0001, 0.01, 0.1, 0.5, 1, 5, 10, 50, 100, 1000]
# q2_21_mse= [0.11441097786804373, 0.11441097786804373, 0.11441097786804373, 0.11441097786804373, 0.11441097786804373, 0.09887498849984258, 0.02707013137678509, 0.004052617150050718, 0.003327907728522661, 0.003051316506525047]
# q6_21_mse= [0.11434260911968593, 0.11434260911968593, 0.11434260911968593, 0.11434260911968593, 0.11434260911968593, 0.10489199880243982, 0.0353235792796044, 0.011995591426155042, 0.010937220246619108, 0.010506290969330229]
# q12_21_mse= [0.11421486188145344, 0.11421486188145344, 0.11421486188145344, 0.11421486188145344, 0.11421486188145344, 0.1138495665545145, 0.048585450409031894, 0.02245557544982397, 0.021176469022388733, 0.020677068594356175]
# q2_107_mse= [0.06063858722775104, 0.06063858722775104, 0.06063858722775104, 0.06063858722775104, 0.06063858722775104, 0.06063858722775104, 0.05000267635644482, 0.0029929594610668296, 0.0014693602715161738, 0.0009577748160919432]
# q6_107_mse= [0.06067003366131637, 0.06067003366131637, 0.06067003366131637, 0.06067003366131637, 0.06067003366131637, 0.06067003366131637, 0.051986148215798625, 0.0057807225759503094, 0.004208736606553893, 0.0036536850158007486]
# q12_107_mse= [0.06071612093265444, 0.06071612093265444, 0.06071612093265444, 0.06071612093265444, 0.06071612093265444, 0.06071612093265444, 0.055375240182499716, 0.010677364564157629, 0.00905771128223078, 0.008307464360183073]
for Ci_l in Ci_range_l:
    lasso = Lasso(alpha=1 / (2 * Ci_l))
    temp_test = []
    temp_train = []
    cv = KFold(n_splits=5)
    for train, test in cv.split(X_train):
        lasso.fit(X_train[train], y_train[train])
        y_pred_l = lasso.predict(X_train[test])
        y_pred_train = lasso.predict(X_train[train])
        temp_test.append(mean_squared_error(y_train[test], y_pred_l))
        temp_train.append(mean_squared_error(y_train[train], y_pred_train))
    mean_error_test.append(np.array(temp_test).mean())
    std_error_test.append(np.array(temp_test).std())
    mean_error_train.append(np.array(temp_train).mean())
    std_error_train.append(np.array(temp_train).std())

print(mean_error_test)
plt.figure()
plt.errorbar(Ci_range_l, mean_error_test, yerr=std_error_test, label="test")
plt.errorbar(Ci_range_l, mean_error_train, yerr=std_error_train,label="train")
plt.xlabel('Ci');
plt.ylabel('Mean square error')
plt.title("Lasso")
plt.legend()
plt.show()

# C_l = 50
C_l = 100
lasso = Lasso(alpha=1 / (2 * C_l)).fit(X_train, y_train)
print("coef lasso")
print(lasso.intercept_, lasso.coef_)
#
test_predictions_lasso = lasso.predict(X_test)
test_results_lasso = pd.DataFrame(data={'Predictions': test_predictions_lasso, 'Target': y_test})
print(test_results_lasso)

printScore(test_results_lasso, "Lasso")
plotResults(test_results_lasso, "Lasso")

