import pandas as pd
import matplotlib.pyplot as plt

station_id = 107
plt.rc('font', size=18)
plt.rcParams['figure.constrained_layout.use'] = True
df = pd.read_csv("datasets/dublinBikes_" + str(station_id) + ".csv", parse_dates=[1])

days=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']

# make plots based on day
for i in range(7):
    dayNumDF = df[(df['DAY_NUMBER'] == i)]
    # get the mean of the num of bikes for each hour
    dayNumDF = dayNumDF.groupby(['HOUR'])['AVAILABLE BIKES'].mean()
    dayNumDF = dayNumDF.reset_index()
    plt.plot(dayNumDF['HOUR'].values, dayNumDF['AVAILABLE BIKES'].values, label=days[i])
    plt.title("Station "+str(station_id))
    plt.xlabel('Hour')
    plt.ylabel('Num. Available Bikes')
    plt.legend()
    plt.show()
