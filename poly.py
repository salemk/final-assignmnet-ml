import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import Ridge

# station_id = 21
from sklearn.preprocessing import PolynomialFeatures

station_id = 107
plt.rc('font', size=18)
plt.rcParams['figure.constrained_layout.use'] = True

df = pd.read_csv("datasets/dublinBikes_" + str(station_id) + ".csv")

df.index = pd.to_datetime((df['TIME']), format='%Y-%m-%d %H:%M:%S')
bikes = df['OCCUPANCY']
day_n = (df['DAY_NUMBER'] + 1) / 7
time = df['TIME']

ST_stride = 1  # short trend stride, every 10 min
week_stride = 24 * 12 * 7  # week trend stride, same time a week ago
day_stride = 24 * 12  # day trend stride, same time a day ago


def short_trend(df, q, lag, dd):
    df_as_np = df.to_numpy()
    short_temp = df[lag * dd - ST_stride * (lag - 1):len(df_as_np) - q - ST_stride * (lag - 1)]
    for i in range(1, lag):
        short_temp2 = df[lag * dd - ST_stride * (lag - 1) + i * ST_stride:len(df_as_np) - q - ST_stride * (
                lag - 1) + i * ST_stride]
        short_temp = np.column_stack((short_temp, short_temp2))
    return short_temp


def week(df, q, lag, dd):
    df_as_np = df.to_numpy()
    week_temp = df[lag * dd + q - week_stride * (lag):len(df_as_np) - week_stride * (lag)]
    for i in range(1, lag):
        week_temp2 = df[lag * dd + q - week_stride * (lag - i):len(df_as_np) - week_stride * (lag - i)]
        week_temp = np.column_stack((week_temp, week_temp2))
    return week_temp


def day(df, q, lag, dd):
    df_as_np = df.to_numpy()
    day_temp = df[lag * dd + q - day_stride * (lag):len(df_as_np) - day_stride * (lag)]
    for i in range(1, lag):
        day_temp2 = df[lag * dd + q - day_stride * (lag - i):len(df_as_np) - day_stride * (lag - i)]
        day_temp = np.column_stack((day_temp, day_temp2))
    return day_temp


k_weight = 'uniform'

# c_ridge = 0.5  # station21
c_ridge= 1.0 #station107

# uncomment below to predict 10min into the future
# q_val = 2
# k_val = 25  # station21
# k_val=10 #station107

# uncomment below to predict half an hour into the future
# q_val = 6
# k_val=50 #station21
# k_val=100 #station107

# uncomment below to predict an hour into the future
q_val = 12
k_val=100

lag_val = 2  # window size
dd_val = 7 * 288  # because I'm using weekly seasonality this gets back exactly one week back at the same time

bikes_y = bikes[lag_val * dd_val + q_val:]
x_short = short_trend(bikes, q_val, lag_val, dd_val)
x_week = week(bikes, q_val, lag_val, dd_val)
x_day = day(bikes, q_val, lag_val, dd_val)
timeDayNum = day_n[lag_val * dd_val + q_val:]
# x_fe = np.column_stack((x_week, x_day, x_short))
x_fe = np.column_stack((x_week, x_short))

# x_fea = PolynomialFeatures(2).fit_transform(x_fe)
# x_fea = PolynomialFeatures(3).fit_transform(x_fe)
x_fea = PolynomialFeatures(4).fit_transform(x_fe)
# x_fea = PolynomialFeatures(5).fit_transform(x_fe)
# x_fea = PolynomialFeatures(6).fit_transform(x_fe)


size_train = int(len(x_fea) * 0.8)
X_train, y_train = x_fea[:size_train], bikes_y[:size_train]
X_test, y_test = x_fea[size_train:], bikes_y[size_train:]


def plotResults(test_result_model, name):
    plt.figure(figsize=(30, 20))
    plt.scatter(test_result_model['Target'].index, test_result_model['Predictions'], color='orange', label="pred")
    plt.scatter(test_result_model['Target'].index, test_result_model['Target'], color='blue', label="actual")
    plt.xlabel("time")
    plt.ylabel("occupancy")
    plt.legend(loc='upper right')
    plt.title(name + " _ Station " + str(station_id))
    plt.show()


def printScore(test_result_model, name):
    print(name + " -  mean squared error: %.2f" % mean_squared_error(test_result_model['Target'],
                                                                     test_result_model['Predictions']))


rigid = Ridge(alpha=1 / (2 * c_ridge)).fit(X_train, y_train)
print("coef")
print(rigid.intercept_, rigid.coef_)

test_predictions = rigid.predict(X_test)
test_results = pd.DataFrame(data={'Predictions': test_predictions, 'Target': y_test})
print(test_results)

printScore(test_results, "Ridge")
plotResults(test_results, "Ridge")


knn = KNeighborsRegressor(n_neighbors=k_val, weights=k_weight)
knn.fit(X_train, y_train)
yPred_knn = knn.predict(X_test)

test_results_knn = pd.DataFrame(data={'Predictions': yPred_knn, 'Target': y_test})
print(test_results_knn)

printScore(test_results_knn, "KNN")
plotResults(test_results_knn, "KNN")

# Baseline model
# assumes that the predicted value would be the same as the last available one.
bikes_y_dummy = bikes[lag_val * dd_val + q_val - q_val:len(bikes) - q_val]
bikes_y_dummy_test = bikes_y_dummy[size_train:]  # get the same slice as the y_test
test_results_dummy = pd.DataFrame(data={'Predictions': np.array(bikes_y_dummy_test), 'Target': y_test})
print(test_results_dummy)

printScore(test_results_dummy, "Baseline Dummy")
# plotResults(test_results_dummy, "Baseline Dummy")

printScore(test_results_dummy, "Baseline Dummy")
plotResults(test_results_dummy, "Baseline Dummy")
