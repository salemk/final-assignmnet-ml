import os

import numpy as np
import pandas as pd

df = pd.read_csv('datasets/dublinbikes_20200101_20200401.csv', parse_dates=[1])
# create a separate csv for each station id
station_id = 21
# station_id = 107
# choose one station in the city center and one in the outsets of the city
# bike station 21 is near tcd while 107 is in the ~outskirts of dublin
df = df[(df['STATION ID'] == station_id)]
df = df[((df['TIME'].dt.strftime('%Y-%m-%d')) >= '2020-01-27') & ((df['TIME'].dt.strftime('%Y-%m-%d')) <= '2020-03-15')]

df.index = pd.to_datetime((df['TIME']), format='%Y-%m-%d %H:%M:%S')
df['OCCUPANCY'] = df['AVAILABLE BIKES'] / df['BIKE STANDS']
df['HOUR'] = df['TIME'].dt.hour

df['Seconds'] = df.index.map(pd.Timestamp.timestamp)
day = 60*60*24

# The day of the week:
# Monday=0, Tuesday =1 , Wednesday =2, Thursday=3, Friday=4, Saturday=5, Sunday=6.
df['DAY_NUMBER'] = df['TIME'].dt.dayofweek

# based on the individual plots for each day, I noticed that tuesday-friday have the same pattern
# while monday, saturday, and sunday have a slightly different one.
# Therefore, I decided to group the similar ones together
df['DAY_TYPE'] = np.where(df['DAY_NUMBER'] == 0, 'Monday', (
    np.where(df['DAY_NUMBER'] < 5, 'Tue-Fri', (np.where(df['DAY_NUMBER'] == 5, 'Saturday', 'Sunday')))))

# join the encoded day type dataframe to the original dataframe
dum_df = pd.get_dummies(df["DAY_TYPE"])
encoded_df = df.join(dum_df)

encoded_df.drop(
    columns=["STATION ID", "LAST UPDATED", "ADDRESS", "NAME", "LATITUDE", "LONGITUDE", "STATUS","DAY_TYPE","Seconds"], axis=1,
    inplace=True)

dir = './datasets'
if not os.path.exists(dir):
    os.mkdir(dir)
filePath = os.path.join(dir, "dublinBikes_" + str(station_id) + ".csv")
encoded_df.to_csv(filePath, index=False)
print("Saving dataset.")
