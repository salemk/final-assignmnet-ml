import pandas as pd
import matplotlib.pyplot as plt

station_id = 107
plt.rc('font', size=18)
plt.rcParams['figure.constrained_layout.use'] = True
df = pd.read_csv("datasets/dublinBikes_" + str(station_id) + ".csv", parse_dates=[0])

# plot all data before filtering on dates in dataExtrac
# plt.scatter(df['TIME'], df['AVAILABLE BIKES'], color='red', marker='.')
# plt.title("Station " + str(station_id))
# plt.show()

dayTypes = ['Monday', 'Tue-Fri', 'Saturday', 'Sunday']

# make plots based on day type
for i in range(4):
    dayTypeDF = df[(df['DAY_TYPE'] == dayTypes[i])]
    # get the mean of the num of bikes for each hour
    dayTypeDF = dayTypeDF.groupby(['HOUR'])['AVAILABLE BIKES'].mean()
    dayTypeDF = dayTypeDF.reset_index()
    plt.figure()
    plt.plot(dayTypeDF['HOUR'].values, dayTypeDF['AVAILABLE BIKES'].values, label=dayTypes[i])
    plt.title("Station " + str(station_id))
    plt.xlabel('Hour')
    plt.ylabel('Num. Available Bikes')
    plt.legend()
    plt.show()
